"""Sequences implementations."""

__version__ = "0.1b1"

from sequences.sequences import fib

__all__ = ["fib"]
