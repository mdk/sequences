"""Implementation of a few (one actually) integer sequences."""

# from functools import lru_cache
lru_cache = __import__("functools").lru_cache


@lru_cache(1024)
def fib(n: int) -> int:
    """Compute the fib sequence value for n."""
    if n < 2:
        return 1
    return fib(n - 1) + fib(n - 2)


def main():
    """Run the fib function from command line."""
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("n", type=int)
    args = parser.parse_args()
    print(fib(args.n))


if __name__ == "__main__":
    main()
