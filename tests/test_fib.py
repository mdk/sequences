from sequences import fib


def test_fib_head():
    assert fib(0) == fib(1) == 1


def test_fib_next():
    assert fib(2) > 1
    assert fib(3) > 1
